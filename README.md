IMPART is a crowdsourcing approach for enabling communities to provide research ideas.
======================================================================================

To use this project follow these steps:

1.  Create your working environment
2.  Install Django
3.  Install additional dependencies
4.  Use the Django admin to create the project
5.  Set up the database

*note: these instructions show creation of a project called "impart".
You should replace this name with the actual name of your project.*

Table of Contents
------------------

[TOC]

Working Environment
-------------------

You have several options in setting up your working environment. We
recommend using virtualenv to separate the dependencies of your project
from your system's python environment. If on Linux or Mac OS X, you can
also use virtualenvwrapper to help manage multiple virtualenvs across
different projects.

### Virtualenv Only

First, make sure you are using virtualenv (<http://www.virtualenv.org>).
Once that's installed, create your virtualenv:

    virtualenv --distribute impart

You will also need to ensure that the virtualenv has the project
directory added to the path. Adding the project directory will allow
django-admin.py to be able to change settings using the --settings flag.

Virtualenv with virtualenvwrapper
---------------------------------

In Linux and Mac OSX, you can install virtualenvwrapper
(<http://virtualenvwrapper.readthedocs.org/en/latest/>), which will take
care of managing your virtual environments and adding the project path
to the site-directory for you:

http://virtualenvwrapper.readthedocs.org/en/latest/install.html
    Add to .bashrc:
        export WORKON_HOME=$HOME/.virtualenvs
        export PROJECT_HOME=$HOME/Devel
        source /usr/local/bin/virtualenvwrapper.sh
    source .bashrc
    mkvirtualenv [name goes here]

Installing Django
-----------------

To install Django in the new virtual environment, run the following
command:

    pip install django


Installation of Dependencies
----------------------------

Depending on where you are installing dependencies:

In development:

    pip install -r requirements/local.txt

For production:

    pip install -r requirements.txt

*note: We install production requirements this way because many
Platforms as a Services expect a requirements.txt file in the root of
projects.*


Use the Django admin to create the project
------------------------------------------

django-admin.py startproject impart


Completing Installation
-----------------------

The last thing you must do after installation is to create a
`secrets.ini` in your `project_name\project_name\settings` folder. The
`secrets.ini` file should look similar to the following:

    # This file contains values that you would like kept out of the repository 
    # for one reason or another.  It is specifically excluded in the .hgignore 
    # and will not appear in the repository.

    [secrets]
    # Use the fab file to generate the secret keys
    SECRET_KEY: GENERATE_USING_FAB_FILE
    CSRF_MIDDLEWARE_SECRET: GENERATE_USING_FAB_FILE

    [database]
    DATABASE_PASSWORD: XXXXXXXX

    [email]
    EMAIL_HOST_PASSWORD: 

    [configfile]
    # This only needs to contain the file name.
    # The path is assumed in the settings.py
    FILE_NAME: local.ini


Changing the Default Settings
-----------------------------

If you would like to change the default settings, please copy and rename the appropriate settings.ini file using the convention `ini_file_name.first_middle_last_initials.ini`.  Then change the pointer in the `secrets.ini` under the `[configfile]` section `FILE_NAME` atrtribute.

To modify the `local.ini` file, you would run the following commands:

    cp local.ini local.Y3I.ini
    replace "local.ini" "local.Y3I.ini" -- secrets.ini


Setting up the Database
-----------------------

I strongly recommend using the same database as your production server instead of the default sqlite.  Here is a setup example using MariaDB:

Install MariaDB

Edit your local.XXX.ini file and change the [database] section to be:

    [database]
    DATABASE_USER: impart_project
    DATABASE_HOST: localhost
    DATABASE_PORT:
    DATABASE_ENGINE: django.db.backends.mysql
    DATABASE_NAME: impart_dev
    TEST_DATABASE_NAME: impart_test

While editing the local.XXX.ini file, also add a [hosts] section:

    [hosts]
    ALLOWED_HOSTS: ['127.0.0.1','localhost']

Create the databases, users, and permissions:

    sudo mysql -h localhost -p -u root
        create database impart_dev;
        create database impart_test;
        create user 'impart_project'@'localhost' identified by 'password';
        grant all privileges on * . * to 'impart_project'@'localhost';

Create the tables for the impart project using ./manage.py migrate:

    ./manage.py migrate

Load the fixtures for the project

    find . -name "*.json" -exec ./manage.py loaddata {} \;

Create your super user:

    ./manage.py createsuperuser


Additional Information
----------------------

Additional information can be found in docs/open_source_setup.md
