impart open source
======================

For initial setup, follow the instructions from the readme file on the Impart Bitbucket page:  https://bitbucket.org/dartmouthinformatics/impart

There are 3 different versions of the application in the impart project.  It is assumed that only one of the three will be used in an implementation.  Though, theoretically one could use more than one without any conflicts.  Please note that the comm_crowd_sourcing and crowd_sourcing share the same user base if you decide to implement both applications.

Description of the Different Applications
-----------------------------------------

crowd_sourcing_open
-------------------
No authentication is required and each form uses captcha.  Questions must be approved before appearing in the list and voting and comments are controlled with cookies.

crowd_sourcing
--------------
Uses the authentication back-end you supply in authentication/backends.  Login is required to submit questions and to comment and vote.  There is no question moderation so questions appear in the list immediately after they're submitted.

comm_crowd_sourcing
-------------------
Uses the authentication back-end you supply in authentication/backends as well as social media authentication including Facebook, LinkedIn, and Google.  Questions must be approved before appearing in the list.


Customization
-------------

In order to use each of the apps, you need to replace the ResearchQuestion model with your own model to collect the data you need.  You will also need to modify forms.py and the related template files to accommodate the new model.  The same goes for the fixtures.

After making the model changes, run ./manage.py makemigrations and ./manage.py migrate and load any fixtures you may have.

You'll need to register your domain with Google recaptcha and obtain a private key to put in your secrets.ini file to reference in your settings.py.

crowd_sourcing_open
-------------------
No further customization should be required.

crowd_sourcing
--------------
You'll have to write your own authentication backend to connect to your institution's authentication system.  Import the module into ImpartAuthBackend.py and replace references to DartLDAP with your own backend.

comm_crowd_sourcing
-------------------
You'll have to write your own authentication backend to connect to your institution's authentication system.  Import the module into ImpartAuthBackend.py and replace references to DartLDAP with your own backend.

For setting up the social media authentication see Impart-python-social-auth.pdf.

