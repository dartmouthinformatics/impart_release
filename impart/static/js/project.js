// Functionality that needs to run at document load.
$(document).ready(function() {

  // Ensure tabbed menus are active
  $('#tabs a').click(function (e) {
    $(this).tab('show');
    handle_tabs(this.hash);
  })
  // This is for jumping to a tab on the same page
  // modified from http://stackoverflow.com/questions/15360112.
  // Requires a class of tab_jump on the sending link.
  $('.tab_jump').click(function(e) {
    // For some reason, this requires preventing default so the page does not scroll to the top of the tabbed section
    e.preventDefault();
    var target_hash = e.target.hash;
    $('#tabs a[href='+target_hash+']').tab('show');
    handle_tabs(target_hash);
  })
  // Ensure that when we directly load a page with tab anchor in url that it opens the tab
  // This does not affect clicking on a tab link on an already loaded page
  if ($('#tabs') && location.hash) {
    $('#tabs a[href='+location.hash+']').tab('show');
    handle_tabs(location.hash);
  }
  // Get back button working with tabbed navigation
  window.onhashchange = function(e) {
    $('#tabs a[href='+location.hash+']').tab('show');
    handle_tabs(location.hash);
    //window.onhashchange = null;
  };

  // Give focus to first form item with a certain class
  $('form.focus_first :input:visible:enabled:first').focus();

  // Enable elements if present
  $('.collapse').collapse();
  $(".droptabs").droptabs();
  $('.dropdown-toggle').dropdown();
  $('.btn').button();

  var original_text = $('#collaborator_form_button').html();
  $('#collaborator_form_button').click(function() {
    $(this).html(function(i, current_txt) {
      return current_txt == original_text ?  'Hide' : original_text;
    });
  });

  // Make all links starting with http have external class
  $("a[href^='http://']:not(a.no_external)").addClass("external");
  $("a[href^='https://']:not(a.no_external)").addClass("external");
  $("a[href^='//']:not(a.no_external)").addClass("external");

  // Make sure all external links open in another window.
  $('a.external,a.no_external').click(function(){
    window.open(this.href);
    return false;
  });


  // Bind the "Lookup my NetID" link on the login page to an
  // AJAX call that places the appropriate NetID in the username
  // textbox or prints the error

  var lookup_result = $('<div id="net_id_result"></div>');
  var email_textbox = $('<div class="row"> \
      <div class="col-xs-9" style="padding-left:10px; padding-right:10px;"><input id="email_textbox" class="form-control input-sm" name="email_textbox" type="text" placeholder="Type your email address" /></div> \
      <div class="col-xs-3" style="padding-left:0px; padding-right:10px;"><button id="lookup_net_id_btn" class="form-control btn btn-default btn-xs" style="height:30px;">Check</button></div> \
    </div>');

  $('#lookup_net_id').popover({
    'html': 'true',
    'placement': 'bottom',
    'content': email_textbox,
    'container': 'body'
  }).on('hidden.bs.popover', function() {
    $(lookup_result).remove();
  }).on('shown.bs.popover', function() {
    $(email_textbox).find('#email_textbox').focus();
  });

  $(email_textbox).find('#email_textbox').on('keyup', function(e) {
    if (e.which == 13) { return lookup_net_id(); }
    if (e.which == 27) { $('#lookup_net_id').popover('hide'); }
  });
  $(email_textbox).find('#lookup_net_id_btn').on('click', function(e) { return lookup_net_id(); });

  function lookup_net_id() {
    var address = $(email_textbox).find('#email_textbox').val();
    if (address.indexOf("@") < 0) {
      address += "@dartmouth.edu";
    }

    $.get('/accounts/get_net_id/', {'email_address': address}, function(data) {
      if (data.error.length == 0) {
        $('#id_username').val(data.netID);
        $('#lookup_net_id').popover('hide');
      }
      else {
        if ($("#net_id_result").length == 0) {
          $(lookup_result).insertAfter(email_textbox).html(data.error);
        } else {
          $(lookup_result).html(data.error);
        }
      }
    });
    return false;
  }
}); // End document load

function handle_tabs(hash) {
  // Expose anchor tags in the URL
  // Modified from http://stackoverflow.com/questions/12131273/
  window.location.hash = hash;
  $('html,body').scrollTop(0);
}
