# -*- coding: utf-8 -*-
"""
Forms for the Crowd Sourcing application.

.. module:: forms
.. moduleauthor:: saj
.. versionadded:: Acai_Berry
"""

import logging

from django.core.validators import MaxLengthValidator
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from autocomplete_light import shortcuts as autocomplete_light
from nocaptcha_recaptcha.fields import NoReCaptchaField
import floppyforms.__future__ as forms

import utils.constants

from .models import ContactMessage
from .models import ResearchQuestion
from .models import ResearchQuestionComment

log = logging.getLogger(__name__)


class ResearchQuestionForm(autocomplete_light.ModelForm):

    """The form for submitting a new :py:class:`ResearchQuestion` question."""

    captcha = NoReCaptchaField(label='')

    error_css_class = 'error'

    def __init__(self, *args, **kwargs):
        """
        Initialize the ResearchQuestionForm object.

        Add the empty string "Please select" option to the dropdown
        boxes for the research question submission form.

        :param *args: Positional arguments to pass to super() (optional).
        :param **kwargs: Keyword arguments to pass to super() (optional).
        """

        super(ResearchQuestionForm, self).__init__(*args, **kwargs)

        self.fields['institution'].choices.pop(0)
        self.fields['institution'].choices.insert(
            0, (u'', utils.constants.EMPTY_LABEL_STR))

        # following line needed to refresh widget copy of choice list
        self.fields['institution'].widget.choices = self.fields[
            'institution'].choices

        # For some reason the model's maxlengthvalidator doesn't work
        # but adding it to the form does the trick.
        self.fields[
            'need_lab_data_specify'].validators = [MaxLengthValidator(1000)]

    def clean(self):
        """
        Clean any fields that depend on each other.

        :returns: Updated dictionary of cleaned data from super().
        :rtype: dict
        """

        cleaned_data = super(ResearchQuestionForm, self).clean()

        cleaned_data = self.clean_departments(cleaned_data)

        cleaned_data = self.clean_dependent_field(
            cleaned_data, 'topic', 'topic_other', u'Other')
        cleaned_data = self.clean_dependent_field(
            cleaned_data, 'institution', 'institution_other', u'Other')
        cleaned_data = self.clean_dependent_field(
            cleaned_data, 'need_lab_data', 'need_lab_data_specify', u'True')

        if cleaned_data['need_lab_data'] == '':
            self._errors['need_lab_data'] = self.error_class(
                [_('This field is required.')])

        return cleaned_data

    def clean_dependent_field(self, cleaned_data, field_name,
                              dependent_name, test_text):
        """
        Clean the other or specify field associated with the passed field.

        :param dict cleaned_data: Cleaned field data to clean further.
        :param string field_name: Name of field that's connected to the
            dependent field
        :param string dependent_name: Name of the field dependent on
            field_name.
        :param string test_text: Text to check for that determines if the
            dependent field should be saved or not.
        :returns: Updated dictionary of cleaned data from super().
        :rtype: dict
        """

        field_value = unicode(cleaned_data.get(field_name))
        # The other/specify text will be missing if it didn't pass the
        # validation so make sure it exists.
        if dependent_name in cleaned_data:
            dependent_value = unicode(cleaned_data.get(dependent_name).strip())
            if field_value == test_text:
                # Make sure the other/specify text is not blank
                if dependent_value == u'':
                    self._errors[dependent_name] = self.error_class(
                        [_(u'This field is required.')])
                else:
                    cleaned_data[dependent_name] = dependent_value
            else:
                cleaned_data[dependent_name] = u''

        return cleaned_data

    def clean_departments(self, cleaned_data):
        """
        Verify that a department was chosen for the selected Institution.

        Check each institution and its respective department list to ensure
        a department has been selected since they are each optional fields
        in the model.

        Make sure an other department is specified if department is "Other."

        :param dict cleaned_data: Cleaned field data to clean further.
        :returns: Updated dictionary of cleaned data from super().
        :rtype: dict
        """
        institution_dept = {
            'Dartmouth Hitchcock Medical Center': 'department_dhmc',
            'Dartmouth College / Geisel': 'department_geisel',
            'Other': 'department_all'
        }
        department_other_flag = False
        for institution, department in institution_dept.iteritems():
            if unicode(cleaned_data.get(
                    'institution')) == unicode(institution):
                if cleaned_data.get(department) is None:
                    self._errors[department] = self.error_class(
                        [_(u'This field is required.')])
                elif unicode(cleaned_data.get(department)) == u'Other':
                    department_other_flag = True
            else:
                # Clear any departments that may had been selected for a
                # different institution.
                cleaned_data[department] = None

        if department_other_flag:
            if unicode(cleaned_data.get('department_other')) == u'':
                self._errors['department_other'] = self.error_class(
                    [_(u'This field is required.')])

        return cleaned_data

    class Meta:

        """Extra information about this object."""

        model = ResearchQuestion
        fields = [
            'first_name',
            'last_name',
            'email',
            'institution',
            'institution_other',
            'department_dhmc',
            'department_geisel',
            'department_all',
            'department_other',
            'topic',
            'topic_other',
            'question',
            'population',
            'key_exposure_of_interest',
            'key_outcome_of_interest',
            'need_lab_data',
            'need_lab_data_specify',
            'ip_address',
        ]
        labels = {
            'topic':
            _('Field of Inquiry for Question'),
            'topic_other': '',
            'institution_other': '',
            'department_dhmc': 'Department',
            'department_geisel': 'Department',
            'department_all': 'Department',
            'department_other': '',
            'question':
            _('What is the research question you would like to examine?'),
            'population':
            _('In what population or disease cohort would you investigate \
            this research question?'),
            'key_exposure_of_interest':
            _('What is the key exposure of interest?'),
            'key_outcome_of_interest':
            _('What is the key outcome of interest?'),
            'need_lab_data':
            _('Please verify that you do not need laboratory (e.g. LDL, creatinine) or biometric \
            data (blood Pressure, weight) to complete a valid study of this \
            question.'),
            'need_lab_data_specify': ''
        }
        help_texts = {
            'question': mark_safe(_(
                """Some Examples:<br />
                <ul>
                <li>&bull; What is the 12 and 24-month mortality of patients
                undergoing below the knee amputation in the U.S? How much does
                this vary by hospital?</li>
                <li>&bull; What is the relative rate of use of percutaneous
                coronary artery intervention (PCI) vs. coronary artery bypass
                graft (CABG) in patients over age 80 compared to patients under
                age 80?</li>
                <li>&bull; What is the incidence of bladder cancer among diabetics
                treated with thiazolidinediones compared to diabetics treated
                with other medications?</li>
                </ul>"""
            )),
            'population': mark_safe(_(
                """Some Examples:<br />
                <ul>
                <li>&bull; Patients hospitalized for below the knee amputations.</li>
                <li>&bull; Patients undergoing percutaneous coronary artery
                intervention and/or coronary artery bypass graft.</li>
                <li>&bull; Diabetics.</li>
                </ul>"""
            )),
            'key_exposure_of_interest': mark_safe(_(
                """Some Examples:<br />
                <ul>
                <li>&bull; Below the knee amputations.</li>
                <li>&bull; Percutaneous coronary artery intervention and/or
                coronary artery bypass graft.</li>
                <li>&bull; Thiazolidinediones and other diabetic drugs</li>
                </ul>"""
            )),
            'key_outcome_of_interest': mark_safe(_(
                """Some Examples:<br />
                <ul>
                <li>&bull; 12-24 month mortality.</li>
                <li>&bull; Bladder cancer.</li>
                </ul>"""
            ))
        }
        widgets = {
            'first_name': forms.TextInput(
                attrs={'size': '50',
                       'class': 'form-control'}),
            'last_name': forms.TextInput(
                attrs={'size': '50',
                       'class': 'form-control'}),
            'email': forms.TextInput(
                attrs={'size': '50',
                       'class': 'form-control'}),
            'institution': forms.Select(
                attrs={'id': 'institution',
                       'class': 'form-control'}),
            # Setting the display to none because the other text box
            # will brielfy show up on the page when it first loads otherwise.
            'institution_other': forms.TextInput(
                attrs={'placeholder': 'Institution, if Other',
                       'style': 'display:none;',
                       'class': 'form-control'}),
            'department_other': forms.TextInput(
                attrs={'placeholder': 'Department, if Other',
                       'style': 'display:none;',
                       'class': 'form-control'}),
            'question': forms.Textarea(attrs={
                'rows': utils.constants.FORMS_TEXTAREA_ROWS,
                'class': 'form-control'}),
            'topic_other': forms.TextInput(
                attrs={'placeholder': 'Field of Inquiry, if Other',
                       'style': 'display:none;',
                       'class': 'form-control'}),
            'population': forms.Textarea(attrs={
                'rows': utils.constants.FORMS_TEXTAREA_ROWS,
                'class': 'form-control'}),
            'key_exposure_of_interest': forms.Textarea(attrs={
                'rows': utils.constants.FORMS_TEXTAREA_ROWS,
                'class': 'form-control'}),
            'key_outcome_of_interest': forms.Textarea(attrs={
                'rows': utils.constants.FORMS_TEXTAREA_ROWS,
                'class': 'form-control'}),
            'need_lab_data': forms.RadioSelect(
                attrs={'id': 'need_lab_data',
                       'class': 'form-control'}),
            'need_lab_data_specify': forms.Textarea(attrs={
                'placeholder': 'If yes, what?',
                'rows': utils.constants.FORMS_TEXTAREA_ROWS,
                'style': 'display:none;',
                'class': 'form-control'
            }),
            'ip_address': forms.HiddenInput()
        }


class ResearchQuestionCommentForm(forms.ModelForm):

    """Form for submitting a :py:class:`ResearchQuestionComment` comment."""

    captcha = NoReCaptchaField(label='')

    class Meta:

        """Extra information about this object."""

        model = ResearchQuestionComment
        fields = [
            'submitted_by',
            'comment',
            'ip_address',
            'question',
            'check_key'
        ]
        labels = {
            'submitted_by': _('Name'),
            'comment': ''
        }
        widgets = {
            'submitted_by': forms.TextInput(attrs={'size': '50'}),
            'comment': forms.Textarea(attrs={
                'rows': utils.constants.FORMS_TEXTAREA_ROWS,
                'class': 'form-control',
                'placeholder': _('Comment is required to vote.')
            }),
            'ip_address': forms.HiddenInput(),
            'question': forms.HiddenInput(),
            'check_key': forms.HiddenInput()
        }


class ContactMessageForm(forms.ModelForm):

    """The form for submitting a new :py:class:`ContactMessage`."""

    captcha = NoReCaptchaField(label='')

    class Meta:

        """Extra information about this object."""

        model = ContactMessage
        fields = [
            'name',
            'email_address',
            'subject',
            'message',
            'cc_myself',
            'ip_address',
        ]
        labels = {
            'cc_myself': _('Send yourself a copy of the email')
        }
        widgets = {
            'name': forms.TextInput(attrs={'size': '50'}),
            'email_address': forms.TextInput(
                attrs={'size': '50'}),
            'subject': forms.TextInput(attrs={'size': '50'}),
            'message': forms.Textarea(attrs={
                'rows': utils.constants.FORMS_TEXTAREA_ROWS,
                'class': 'form-control'
            }),
            'cc_myself': forms.CheckboxInput(),
            'ip_address': forms.HiddenInput(),
        }
