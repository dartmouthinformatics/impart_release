# -*- coding: utf-8 -*-
"""
Collection of constants for use throughout the project.

.. moduleauthor:: sba
.. moduleauthor:: saj
.. versionadded:: Acai_Berry
"""

# Names to be embedded throughout site.
WEBSITE_NAME = 'Impart'
PUBLIC_WEBSITE_NAME = 'Dartmouth SYNERGY'

# Forms display
FORMS_TEXTAREA_ROWS = 3
EMPTY_LABEL_STR = 'Please select'
OPTIONAL_EMPTY_LABEL_STR = 'None'

# Forms template display; note that these need to be loaded in the context
# processor as well to make them available in the templates
FORMS_LEFT_GUTTER = 1

# Navigation items in Impart
IMPART_CANCEL = 'Cancel'
IMPART_CONFIRM = 'Confirm'
IMPART_CONTINUE = 'Continue'
IMPART_SUBMIT = 'Submit'
IMPART_UPDATE = 'Update'
IMPART_SUBMIT_QUESTION = 'Submit your question'
IMPART_VIEW_QUESTIONS = 'View questions'
IMPART_VIEW_SUBMITTED_QUESTIONS = 'View submitted questions'
