# -*- coding: utf-8 -*-
"""
Custom template to create obfuscated mailtos.

Uses JS to "encrypt" the address and inject the mailto tag
on the fly.

Heavily modified from https://djangosnippets.org/snippets/1907/.

Extended 2014.05 to allow for passing in a separate label string
as the display text.

.. note::
    If this is to be used in content being pulled from a database entry and
    rendered by our custom renderer, then the load must be done inside the
    block of text being pulled from the database. Otherwise, it is not seen.

.. moduleauthor:: sba
.. versionadded:: Tecumseh
"""

import re
from django import template


register = template.Library()


class EncryptEmail(template.Node):

    """
    Obfuscate an email address with JavaScript code.
    """

    def __init__(self, context_var, context_label=None):
        """
        Set context variables.

        :param str context_var: The email address to obsuscate.
        :param context_label: Optional display label.
        :type: str or None
        """

        self.context_var = template.Variable(context_var)
        self.context_label = None
        if context_label:
            self.context_label = template.Variable(context_label)

    def render(self, context):
        """
        Create the JavaScript code to hide an email address.

        :param Context context: The context in which to render this tag.
        :returns: Script to embed
        :rtype: str
        """

        import random
        email_address = self.context_var.resolve(context)
        email_label = None
        if self.context_label:
            email_label = self.context_label.resolve(context)
        character_set = '+-.0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        character_set += '_abcdefghijklmnopqrstuvwxyz'
        char_list = list(character_set)
        random.shuffle(char_list)

        key = ''.join(char_list)

        cipher_text = ''
        id = 'e' + str(random.randrange(1,999999999))

        for a in email_address:
            cipher_text += key[ character_set.find(a) ]

        script = 'var a="'+key+'";var b=a.split("").sort().join("");var c="'+cipher_text+'";var d="";'
        script += 'for(var e=0;e<c.length;e++)d+=b.charAt(a.indexOf(c.charAt(e)));'
        if email_label:
            script += 'document.getElementById("'+id+'").innerHTML="<a href=\\"mailto:"+d+"\\">'+email_label+'</a>"'
        else :
            script += 'document.getElementById("'+id+'").innerHTML="<a href=\\"mailto:"+d+"\\">"+d+"</a>"'


        script = "eval(\""+ script.replace("\\","\\\\").replace('"','\\"') + "\")"
        script = '<script type="text/javascript">/*<![CDATA[*/'+script+'/*]]>*/</script>'

        return '<span id="'+ id + '">[javascript protected email address]</span>'+ script


def encrypt_email(parser, token):
    """
    {% encrypt_email user.email %}

    Uses email as displayed text for mailto:.

    :param Parser parser: Default parser.
    :param str token: Contents of the tag.
    :returns: Script to embed.
    :rtype: str
    :raises template.TemplateSyntaxError: Ill-constructed tag.
    """

    tokens = token.contents.split()
    if len(tokens) != 2:
        raise template.TemplateSyntaxError(
            "{0} tag accepts two arguments".format(tokens[0]))
    return EncryptEmail(tokens[1])

def encrypt_email_with_label(parser, token):
    """
    {% encrypt_email user.email label %}

    Uses label as displayed text for mailto:.

    .. note::
       The label cannot have any embedded white spaces, since the tokenizer
       uses that to split off tokens. Use a non-breaking space character
       (&nbsp;) to put in phrases.

    :param Parser parser: Default parser.
    :param str token: Contents of the tag.
    :returns: Script to embed.
    :rtype: str
    :raises template.TemplateSyntaxError: Ill-constructed tag.
    """

    tokens = token.contents.split()
    if len(tokens) != 3:
        raise template.TemplateSyntaxError(
            "{0} tag accepts three arguments".format(tokens[0]))
    return EncryptEmail(tokens[1], tokens[2])


register.tag('encrypt_email', encrypt_email)
register.tag('encrypt_email_with_label', encrypt_email_with_label)
