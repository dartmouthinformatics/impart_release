# -*- coding: utf-8 -*-
"""
Collection of mixins for use throughout the project.

.. moduleauthor:: sba
.. versionadded:: Acai_Berry
"""

from django.contrib import messages


class ChoiceFieldRepresentationsMixin(object):

    """
    Utility mixin for FieldChoice objects.

    Include the string, repr, and unicode
    representations for a FieldChoice, since these
    always use the same field.
    """

    def __str__(self):
        """
        Conversion of object for string representation.

        :returns: String representation of object.
        :rtype: str
        """

        return self.name

    def __repr__(self):
        """
        Returns a representation of the object.

        :returns: Raw representation of object.
        :rtype: str
        """

        return "{0}, {1}".format(self.id, self.name)

    def __unicode__(self):
        """
        Conversion of object for string representation.

        :returns: Unicode representation of object.
        :rtype: str
        """

        return unicode(self.__str__())


class ChangeActionMessageMixin(object):

    """
    Adds flash message for creation or update.

    Per Two Scoops, create a mixin to allow using CBV with the list
    views as a confirmation page, by adding in a flash message.
    """

    @property
    def success_msg(self):
        """
        Add a property indicating whether this object was created or updated.

        Returns the NotImplemented singleton rather than raising a
        NotImplementedError per Two Scoops.

        :returns: Indicator that the message property not implemented in class.
        :rtype: NotImplemented
        """

        return NotImplemented

    def form_valid(self, form):
        """
        Add flash message.

        Per Two Scoops, extend form_valid to update the User when they
        either create or update an object and are then taken to the
        DetailView.

        :param Form form: The form associated with this view.
        :returns: Destination of this view, with flash messaged added.
        :rtype: HttpResponse
        """

        self.add_flash_message()
        return super(ChangeActionMessageMixin, self).form_valid(form)

    def add_flash_message(self):
        """
        Insert a message string into the messages object.
        """

        messages.success(self.request, self.success_msg)
