# -*- coding: utf-8 -*-
"""
Run tests for the Utils application.

.. moduleauthor:: sba
.. versionadded:: Acai_Berry
"""

# import datetime

from django import template
from django.test import TestCase

from .fields import FullNameTextBoxesField
from .widgets import FullNameTextBoxesWidget
from . import context_processors


class ContextProcessorsTestCase(TestCase):

    """
    Test the context processors for this module.
    """

    def setUp(self):
        """
        None yet.
        """

        pass

    def test_web_constants(self):
        """
        Should return the constants representing aspects of the website.

        Not testing all of them, just enough to know the setup is working.
        """

        self.assertEqual(
            context_processors.web_constants(None)['WEB_SYSTEM_NAME'],
            'Impart')
        self.assertEqual(
            context_processors.web_constants(None)['IMPART_CANCEL'],
            'Cancel')


class ChoiceFieldRepresentationsMixinTestCase(TestCase):

    """
    NOOP: This mixin is tested in the classes which use it.
    """

    pass  # Has no fields on which to do the testing

    def setUp(self):
        """
        None yet.
        """

        pass

    def test_dummy_mock(self):
        """
        Run a mock test so we get the setUp code run for coverage.

        Remove this if we add real tests.
        """

        pass


class EncryptEmailTemplateTagTestCase(TestCase):

    """
    Test the templatetag to obfuscate email addresses.
    """

    def setUp(self):
        """
        Create some default objects for testing.
        """

        self.email_address = 'test@example.com'
        self.email_label = 'Some String'

    def test_encrypt_email(self):
        """
        Test the templatetag.
        """

        output_str = template.Template(
            "{% load obfuscate_email_address %}"
            "{% encrypt_email address %}"
        ).render(template.Context({
            'address': self.email_address,
        }))

        # Cannot do an exact match, since it is "encrypted" with a random
        # sequence of characters each time
        self.assertTrue(output_str.startswith('<span id='))
        self.assertTrue(output_str.endswith('/*]]>*/</script>'))
        self.assertFalse(self.email_address in output_str)

    def test_encrypt_email_with_no_parameter(self):
        """
        Test the templatetag with a bogus extra parameter.

        Should throw exception.
        """

        with self.assertRaises(template.TemplateSyntaxError):
            template.Template(
                "{% load obfuscate_email_address %}"
                "{% encrypt_email %}"
            ).render(template.Context({
                'address': self.email_address,
                'bogus': 'bogus str'
            }))

    def test_encrypt_email_extra_parameter(self):
        """
        Test the templatetag with a bogus extra parameter.

        Should throw exception.
        """

        with self.assertRaises(template.TemplateSyntaxError) as exc:
            template.Template(
                "{% load obfuscate_email_address %}"
                "{% encrypt_email address bogus %}"
            ).render(template.Context({
                'address': self.email_address,
                'bogus': 'bogus str'
            }))

        self.assertEqual(exc.exception.message,
                         "encrypt_email tag accepts two arguments")

    def test_encrypt_email_with_label(self):
        """
        Test the templatetag that allows for a display label.
        """

        output_str = template.Template(
            "{% load obfuscate_email_address %}"
            "{% encrypt_email_with_label address email_label %}"
        ).render(template.Context({
            'address': self.email_address,
            'email_label': self.email_label,
        }))

        # Cannot do an exact match, since it is "encrypted" with a random
        # sequence of characters each time
        self.assertTrue(output_str.startswith('<span id='))
        self.assertTrue(output_str.endswith('/*]]>*/</script>'))
        self.assertTrue(self.email_label in output_str)

    def test_encrypt_email_with_label_using_strings(self):
        """
        Test the templatetag that allows for a display label as string.
        """

        output_str = template.Template(
            "{% load obfuscate_email_address %}"
            "{% encrypt_email_with_label 't@example.com' 'e_label' %}"
        ).render(template.Context())

        # Cannot do an exact match, since it is "encrypted" with a random
        # sequence of characters each time
        self.assertTrue(output_str.startswith('<span id='))
        self.assertTrue(output_str.endswith('/*]]>*/</script>'))
        self.assertFalse('t@example.com' in output_str)
        self.assertTrue('e_label' in output_str)

    def test_encrypt_email_with_label_extra_parameter(self):
        """
        Test the templatetag with a bogus extra parameter.

        Should throw exception.
        """

        with self.assertRaises(template.TemplateSyntaxError) as exc:
            template.Template(
                "{% load obfuscate_email_address %}"
                "{% encrypt_email_with_label address email_label bogus %}"
            ).render(template.Context({
                'address': self.email_address,
                'email_label': self.email_label,
                'bogus': 'bogus str',
            }))

        self.assertEqual(
            exc.exception.message,
            "encrypt_email_with_label tag accepts three arguments")


class FullNameTextBoxesFieldTestCase(TestCase):

    """
    Test the custom field.
    """

    def setUp(self):
        """
        Create the objects needed for testing.
        """

        self.field = FullNameTextBoxesField()

    def test_field(self):
        """
        Test the initialization of the field.
        """

        self.assertEqual(len(self.field.fields), 2)
        self.assertIsNotNone(self.field.widget)

    def test_compress(self):
        """
        Test the compress method.
        """

        my_list = ['FirstValue', 'SecondValue']
        self.assertEqual(self.field.compress(my_list), my_list)


class FullNameTextBoxesWidgetTestCase(TestCase):

    """
    Test the custom widget.
    """

    def setUp(self):
        """
        Create the objects needed for testing.
        """

        self.expected_str = '<label for="first_name">First Name:'
        self.expected_str += '<input type="text" name="full_name_0"'
        self.expected_str += ' value="first_name">'
        self.expected_str += '</label>'
        self.expected_str += '<label for="last_name">Last Name:'
        self.expected_str += '<input type="text" name="full_name_1"'
        self.expected_str += ' value="last_name">'
        self.expected_str += '</label>'

        self.input_str = u'<input type="text" name="full_name_0"'
        self.input_str += ' value="first_name">\n'
        self.input_str += '<input type="text" name="full_name_1"'
        self.input_str += ' value="last_name">\n'

    def test_widget(self):
        """
        Test the initialization of the widget.
        """

        widget = FullNameTextBoxesWidget()
        my_values = ['first_name', 'last_name']
        rendered_widgets = super(
            FullNameTextBoxesWidget, widget).render(
            'full_name', my_values)

        self.assertEqual(rendered_widgets, self.input_str)
        self.assertEqual(len(widget.widgets), 2)

    def test_decompress(self):
        """
        Test the decompress method.
        """

        widget = FullNameTextBoxesWidget()
        my_values = ['first_name', 'last_name']

        self.assertEqual(widget.decompress(my_values), my_values)
        self.assertEqual(widget.decompress([]), [None, None])

    def test_format_output(self):
        """
        Test the output formatter method.
        """

        widget = FullNameTextBoxesWidget()
        my_values = ['first_name', 'last_name']
        rendered_widgets = super(
            FullNameTextBoxesWidget, widget).render(
            'full_name', my_values)

        self.assertEqual(
            widget.format_output(rendered_widgets), self.input_str)
