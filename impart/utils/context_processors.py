# -*- coding: utf-8 -*-
"""
Collection of context processors which should be available to all applications.

.. moduleauthor:: sba
.. versionadded:: Acai_Berry
"""

import constants


def web_constants(request):
    """
    Constants representing elememts of the system.

    :param Request request: Request associated with this Context.
    :returns: Linkage between Context and Processor.
    :rtype: dict
    """

    return {
        'EMPTY_LABEL_STR': constants.EMPTY_LABEL_STR,
        'FORMS_LEFT_GUTTER': constants.FORMS_LEFT_GUTTER,
        'IMPART_CANCEL': constants.IMPART_CANCEL,
        'IMPART_CONFIRM': constants.IMPART_CONFIRM,
        'IMPART_CONTINUE': constants.IMPART_CONTINUE,
        'IMPART_SUBMIT': constants.IMPART_SUBMIT,
        'IMPART_UPDATE': constants.IMPART_UPDATE,
        'IMPART_SUBMIT_QUESTION': constants.IMPART_SUBMIT_QUESTION,
        'IMPART_VIEW_QUESTIONS': constants.IMPART_VIEW_QUESTIONS,
        'IMPART_VIEW_SUBMITTED_QUESTIONS': constants.IMPART_VIEW_SUBMITTED_QUESTIONS,
        'PUBLIC_WEBSITE_NAME': constants.PUBLIC_WEBSITE_NAME,
        'WEB_SYSTEM_NAME': constants.WEBSITE_NAME,
    }
