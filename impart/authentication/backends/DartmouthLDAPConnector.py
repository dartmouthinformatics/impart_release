"""
Django module to interact with Dartmouth LDAP.

Allows authentication of and creation of users and groups via the Dartmouth LDAP
system.

.. module:: dartmouthldapconnector
    :synopsis: Dartmouth LDAP authentication module.

.. moduleauthor:: Brian Levin
.. moduleauthor:: Siobhan Jacobson
"""
import ldap
import logging

log = logging.getLogger(__name__)


class DartLDAP(object):

    """
    Authenticates against Dartmouth College LDAP.

    Use the login name, and a hash of the password.
    """

    def __init__(self):
        """ Set the basic parms based on an expected return value. """
        self.UserName = ''
        self.Password = ''
        self._Server_ = 'ldaps://oud.dartmouth.edu'
        self._BaseDn_ = 'dc=dartmouth,dc=edu'
        self._UserEntry_ = None
        self._UserCN_ = None
        self._Auth_Success_ = False
        self._LDAPUserAttribute_ = 'uid'
        self._UserInfo_ = None
        self._EntryCount_ = None

    def __repr__(self):
        """Print a programmer readable class value."""
        return ('Server: {0}, Filter: ({1}={2}), '
                'User DN: {3}, User CN: {4}').format(
                    self._Server_,
                    self._LDAPUserAttribute_,
                    self.UserName,
                    self._UserCN_,
                    self._UserEntry_)

    def __str__(self):
        """Print a person readable class value."""
        return 'Dartmouth LDAP Authenication'

    def __unicode__(self):
        """Print a django readable class value."""
        return unicode(self.__str__())

    def _get_userdn_(self):
        """
        Get userdn from Dartmouth LDAP.

        Connect to the Dartmouth LDAP Server and
        get a userdn value and format it correctly.
        Output that value to the _UserEntry_ variable.
        """
        Connection = ldap.initialize(self._Server_)
        _Filter_ = '{0}={1}'.format(self._LDAPUserAttribute_, self.UserName)
        Connection.simple_bind_s()
        try:
            self._UserEntry_ = Connection.search_s(
                self._BaseDn_, ldap.SCOPE_SUBTREE, _Filter_)

        except Exception as e:
            log.debug(
                "Exception:{0}".format(e))

        else:
            Connection.unbind_s()

        finally:
            return self._UserEntry_

    def _ldab_bind_(self):
        """
        Bind to Dartmouth LDAP.

        Connect to the Dartmouth LDAP Server and
        present the UserName and Password for authentication.
        If the password accepted set _Auth_Success_ to True
        otherwise set _Auth_Success_ to False.
        """
        if self._UserEntry_:
            self._UserCN_ = self._UserEntry_[0][0]

            try:
                Connection = ldap.initialize(self._Server_)
                Connection.simple_bind_s(self._UserCN_, self.Password)

            except Exception as e:
                log.debug('Exception: {0}'.format(e))

            else:
                self._Auth_Success_ = True
                Connection.unbind_s()

        return self._Auth_Success_

    def _set_user_info_(self):
        """
        Set user_info to correct LDAP values.

        Parse LDAP schema and finds correct LDAP values associated
        with the username.  Then parse the LDAP entry into a dictionary.
        """
        self._UserInfo_ = []

        for user in self._UserEntry_:
            info = {}
            info['first_name'] = user[1]['givenName'][0]
            info['last_name'] = user[1]['sn'][0]
            info['email_address'] = user[1]['mail'][0]
            info['group'] = user[1]['eduPersonPrimaryAffiliation'][0]
            info['source'] = 'Dartmouth'
            info['uid'] = user[1]['uid'][0].upper()
            self._UserInfo_.append(info)
            log.debug('User Information: {0}'.format(self._UserInfo_))

        self._EntryCount_ = len(self._UserEntry_)

    def _is_password_empty_(self):
        return False if self.Password else True

    def get_user_attributes(self, username, attribute='mail'):

        self.UserName = username
        self._LDAPUserAttribute_ = attribute

        log.info(
            'Checking to see if user {0} exists.'.format(self.UserName))
        if self._get_userdn_():
            log.info('User {0} exist.'.format(self.UserName))
            self._ldab_bind_()
            self._set_user_info_()
        else:
            log.info('User {0} does NOT exist.'.format(self.UserName))

        log.debug('Count: {0}, User: {1}'.format(
            self._EntryCount_, self._UserInfo_))

        return (self._EntryCount_, self._UserInfo_)

    def authenticate_user_and_get_attributes(self, username, password):

        self.UserName = username
        self.Password = password
        log.info(
            'Checking to see password is empty for {0}.'.format(self.UserName))
        if self._is_password_empty_():
            return (self._Auth_Success_, self._UserInfo_)

        log.info('Checking to see if user {0} exists.'.format(self.UserName))
        if self._get_userdn_():
            log.info('User {0} exists.'.format(self.UserName))
        else:
            log.info('User {0} does NOT exist.'.format(self.UserName))
            return (self._Auth_Success_, self._UserInfo_)

        log.info(
            'Checking to see password is correct for {0}.'.format(self.UserName))
        if self._ldab_bind_():
            log.info(
                'User {0} successfully authenticated? {1}'.format(self.UserName, self._Auth_Success_))
            self._set_user_info_()
            return (self._Auth_Success_, self._UserInfo_[0])
        else:
            log.info('User {0} did NOT successfully authenticate. Bad password!'.format(
                self.UserName))
            return (self._Auth_Success_, self._UserInfo_)


def main():
    import getpass
    # reload(logging)
    logging.basicConfig(
        format='%(asctime)s %(levelname)s:%(message)s', level=logging.DEBUG, datefmt='%I:%M:%S')
    TestAuth = DartLDAP()
    Name = str(raw_input('Please enter your username: '))
    PassW = getpass.getpass('Please enter your password: ')
    return TestAuth.authenticate_user_and_get_attributes(Name, PassW)


if __name__ == '__main__':
    main()
