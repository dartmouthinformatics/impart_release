"""
Django module to interact with Dartmouth LDAP.

Allows authentication of and creation of users and groups via the Dartmouth LDAP
system.

.. module:: ImpartAuthBackend
    :synopsis: Dartmouth LDAP authentication module.

.. moduleauthor:: Brian Levin
.. moduleauthon:: Siobhan Jacobson
"""
import logging

from django.contrib.auth.models import User, Group
from django.core.exceptions import PermissionDenied

from authentication.backends.DartmouthLDAPConnector import DartLDAP

log = logging.getLogger(__name__)


class DartBackend(object):

    """
    Authenticates against Dartmouth College LDAP.

    Use the login name, and a hash of the password.
    """

    def __init__(self):
        """ Set the basic parms based on an expected return value. """
        self.__UserName__ = None
        self.Password = None
        self._user_info_ = None

    @property
    def UserName(self):
        """
        Create a getter for the private __UserName__ property.

        Really for the sole reason that we want to set a setter.
        See next comment.
        """
        return self.__UserName__

    @UserName.setter
    def UserName(self, username):
        """
        Create a setter that saves __UserName__ in all capital letters.

        This is necessary because django is case sensitive and LDAP is not.
        """
        self.__UserName__ = username.upper()

    def __repr__(self):
        """Print a programmer readable class value."""
        return self.__str__()

    def __str__(self):
        """Print a person readable class value."""
        return 'Inspire Authentication Module'

    def __unicode__(self):
        """Print a django readable class value."""
        return unicode(self.__str__())

    def _get_or_create_username_(self):
        """
        Get or create the django user.

        Try to get the user. If the user does not
        exist, creates user and assigns it appropriate values.
        Saves user to the django instance.
        """
        try:
            user = User.objects.get(username=self.UserName)
        except User.DoesNotExist:
            log.debug(
                'User does not exist in Impart. Trying to create user {0}.'.format(
                self.UserName))
            user = User(
                username=self.UserName, first_name=self._user_info_[
                    'first_name'],
                last_name=self._user_info_[
                    'last_name'], email=self._user_info_['email_address'])
            user.is_staff = False
            user.set_unusable_password()
            user.save()
            user_group, created = Group.objects.get_or_create(
                name=self._user_info_['group'])
            user_group.save()
            user.groups.add(user_group)
            user.save()

        return user

    def authenticate(self, username=None, password=None, source=None):
        """
        Authenticate username and password against Dartmouth LDAP.

        Check to make sure that the username and password exist.
        Authenticate username and password against Dartmouth LDAP.
        Allow user to enter system if already created otherwise
        create user.
        """
        self.UserName = username
        self.Password = password
        self.Source = 'Dartmouth'

        log.debug('User {0} has attempted to login. Handing login request to LDAP Connector'.format(
            self.UserName))

        dart_auth = DartLDAP()

        auth_succes, self._user_info_ = dart_auth.authenticate_user_and_get_attributes(
            self.UserName, self.Password)

        if auth_succes:
            return self._get_or_create_username_()
        else:
            log.error('Invalid login: {0}'.format(self.UserName))
            raise PermissionDenied

    def get_user(self, user_id):
        """Get the django user object or if the user does not exist returns None."""
        try:
            log.info('UserID {0} was recieved'.format(user_id))
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            log.warn('Invalid login: {0}'.format(self.UserName))
            return None


def main():
    """
    Direct access to the authentication library for django.

    Allow user to directly add users from the Dartmouth LDAP
    system to the django authentication system.
    May be used with or without user password.
    """
    import getpass
    # reload(logging)
    logging.basicConfig(
        format='%(asctime)s %(levelname)s:%(message)s',
        level=logging.DEBUG, datefmt='%I:%M:%S')
    Name = str(raw_input('Please enter your username: '))
    PassW = getpass.getpass('Please enter your password: ')
    testAuth = DartBackend()
    return testAuth.authenticate(Name, PassW)


if __name__ == '__main__':
    main()
