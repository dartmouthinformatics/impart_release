# -*- coding: utf-8 -*-
"""
Forms for authentication.

.. moduleauthor:: bl
.. versionadded:: Acai_Berry
"""
import logging

from utilities import remove_ratelimit_key
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _

import floppyforms as forms

log = logging.getLogger(__name__)


class ImpartAuthenticationForm(AuthenticationForm):

    """Login form for Impart."""

    error_messages = {
        'invalid_login': _(u'Invalid NetID or Password'),
        'inactive': _(u'This account is inactive.'),
    }
    error_css_class = 'error'

    def __init__(self, *args, **kwargs):
        """
        Set 'NetID' as the label for the username field (Dartmouth-specific).

        :param args: An optional list of positional arguments to pass to super().
        :type args: list
        :param kwargs: An optional dictionary of keyword arguments to pass to super().
        :type kwargs: dict
        """
        super(ImpartAuthenticationForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = _(u'NetID')


class RemoveRateLimitForm(forms.Form):

    """
    Form used to manually delete a rate-limit key from the cache
    in the event that a legitimate user is rate-limited and needs to
    login.

    Note: rate-limit keys are stored in the format: 'rl:127.0.0.1' but the
    form only requires the IP address to be entered. The 'rl:' will be
    concatenated on in the remove_ratelimit_key function when called.
    """

    IP = forms.GenericIPAddressField()

    def __init__(self, *args, **kwargs):
        """
        Set 'IP Address' as the label for the IP field.
        Make IP Address field required.

        :param args: An optional list of positional arguments to pass to super().
        :type args: list
        :param kwargs: An optional dictionary of keyword arguments to pass to super().
        :type kwargs: dict
        """
        super(RemoveRateLimitForm, self).__init__(*args, **kwargs)
        self.fields['IP'].label = _(u'IP Address')
        self.fields['IP'].required = True

    def remove_key(self):
        """
        Clean the data contained in the 'IP' field and pass it into the
        remove_ratelimit_key function, where it will be removed if found
        in the cache.
        """
        cleaned_data = super(RemoveRateLimitForm, self).clean()
        remove_ratelimit_key(cleaned_data.get('IP'))
