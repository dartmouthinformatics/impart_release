# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='authentication_source',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('source_text', models.CharField(max_length=50, verbose_name=b'Authentication Source')),
            ],
            options={
                'verbose_name': 'Authentication Source',
                'verbose_name_plural': 'Authentication Sources',
            },
        ),
        migrations.CreateModel(
            name='DartProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dart_department', models.CharField(max_length=100)),
                ('dart_physical_address', models.CharField(max_length=10)),
                ('source_of_authentication', models.ForeignKey(to='authentication.authentication_source')),
                ('user', models.OneToOneField(related_name='user_ext', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
