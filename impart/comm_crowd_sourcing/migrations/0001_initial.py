# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import model_utils.fields
import django.utils.timezone
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('crowd_sourcing', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ContactMessage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('name', models.CharField(max_length=100)),
                ('email_address', models.EmailField(max_length=254, validators=[django.core.validators.EmailValidator()])),
                ('subject', models.CharField(max_length=100)),
                ('message', models.TextField(max_length=1000, validators=[django.core.validators.MaxLengthValidator(1000)])),
                ('cc_myself', models.BooleanField(default=False)),
                ('ip_address', models.GenericIPAddressField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='FAQ',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('question', models.CharField(max_length=1000, validators=[django.core.validators.MaxLengthValidator(1000)])),
                ('answer', models.CharField(max_length=1000, validators=[django.core.validators.MaxLengthValidator(1000)])),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ResearchQuestion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('institution', models.CharField(max_length=35, choices=[(b'Dartmouth College / Geisel', b'Dartmouth College / Geisel'), (b'Dartmouth Hitchcock Medical Center', b'Dartmouth Hitchcock Medical Center'), (b'Other', b'Other')], validators=[django.core.validators.MaxLengthValidator(35)])),
                ('institution_other', models.CharField(default=b'', max_length=50, blank=True)),
                ('department_other', models.CharField(default=b'', max_length=50, blank=True)),
                ('question', models.TextField(max_length=1000, validators=[django.core.validators.MaxLengthValidator(1000)])),
                ('population', models.TextField(max_length=1000, validators=[django.core.validators.MaxLengthValidator(1000)])),
                ('key_exposure_of_interest', models.TextField(max_length=1000, validators=[django.core.validators.MaxLengthValidator(1000)])),
                ('key_outcome_of_interest', models.TextField(max_length=1000, validators=[django.core.validators.MaxLengthValidator(1000)])),
                ('need_lab_data', models.BooleanField(default=0, choices=[(0, b'No, I do not need laboratory or biometric data.'), (1, b'Yes, I do need laboratory or biometric data.')])),
                ('need_lab_data_specify', models.TextField(default=b'', max_length=1000, blank=True, validators=[django.core.validators.MaxLengthValidator(1000)])),
                ('topic_other', models.CharField(default=b'', max_length=50, blank=True)),
                ('approved', models.BooleanField(default=False)),
                ('ip_address', models.GenericIPAddressField()),
                ('view_count', models.IntegerField(default=0)),
                ('department_all', models.ForeignKey(related_name='dept_all_comm', default=b'', blank=True, to='crowd_sourcing.AllDeptChoiceField', null=True)),
                ('department_dhmc', models.ForeignKey(related_name='dept_dhmc_comm', default=b'', blank=True, to='crowd_sourcing.DHMCDeptChoiceField', null=True)),
                ('department_geisel', models.ForeignKey(related_name='department_comm', default=b'', blank=True, to='crowd_sourcing.GeiselDeptChoiceField', null=True)),
                ('topic', models.ForeignKey(related_name='topic_comm', to='crowd_sourcing.ResearchQuestionTopicChoiceField')),
                ('user', models.ForeignKey(related_name='user_question_comm', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ResearchQuestionComment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('comment', models.TextField(max_length=1000, validators=[django.core.validators.MaxLengthValidator(1000)])),
                ('ip_address', models.GenericIPAddressField()),
                ('question', models.ForeignKey(to='comm_crowd_sourcing.ResearchQuestion')),
                ('user', models.ForeignKey(related_name='user_comment_comm', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ResearchQuestionVote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('ip_address', models.GenericIPAddressField()),
                ('vote', models.BooleanField(default=0)),
                ('question', models.ForeignKey(to='comm_crowd_sourcing.ResearchQuestion')),
                ('user', models.ForeignKey(related_name='user_vote_comm', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
