# -*- coding: utf-8 -*-
"""
Strip html tags and put back apostrophes.

Primarily used for emails.

.. moduleauthor:: sba
.. versionadded:: Acai_Berry
"""

# import bleach
# import string

# from django import template


# register = template.Library()


# @register.simple_tag
# def bleach_strip(str):
#     """
#     Strip html tags and un-encode apostrophes, quotes, etc.

#     Uses bleach to do the stripping.

#     :param str str: The :py:class:`Section` name.
#     :returns: Text to insert into email.
#     :rtype: str
#     """

#     clean_str = string.replace(str, '&#39;', "'")
#     clean_str = string.replace(str, '&quot;', '"')
#     return bleach.clean(clean_str, strip=True)
