# -*- coding: utf-8 -*-
"""
Routing for the Crowd Sourcing application.

.. moduleauthor:: sba
.. moduleauthor:: saj
.. versionadded:: Acai_Berry
"""

from django.conf.urls import url
from .views import AboutView

from .views import ContactMessageCreateView
from .views import FAQView
from .views import HomePageDetailView
from .views import ResearchQuestionCommentCreateView
from .views import ResearchQuestionCreateView
from .views import ResearchQuestionListView
from .views import ThankYouContactView
from .views import ThankYouView
from .views import UnsubscribeConfirmView
from .views import UnsubscribeEmailUpdateView

app_name = 'crowd_sourcing'

urlpatterns = [
    url(
        r'^$$',
        HomePageDetailView.as_view(),
        name='index'
        ),
    url(
        r'^research_question/$',
        ResearchQuestionCreateView.as_view(),
        name='create'
        ),
    url(
        r'^list/(?P<topic_filter>\w+)$',
        ResearchQuestionListView.as_view(),
        name='list_filter'
        ),
    url(
        r'^list/$',
        ResearchQuestionListView.as_view(),
        name='list'
        ),
    url(
        r'^detail/(?P<pk>\d+)$',
        ResearchQuestionCommentCreateView.as_view(),
        name='comment_create'
        ),
    url(
        r'^thank_you/$',
        ThankYouView.as_view(),
        name='thank_you'
        ),
    url(r'^about/',
        AboutView.as_view(),
        name='about'
        ),
    url(
        r'^contact_message/$',
        ContactMessageCreateView.as_view(),
        name='contact'
        ),
    url(
        r'^thank_you_contact/$',
        ThankYouContactView.as_view(),
        name='thank_you_contact'
        ),
    url(
        r'^faq/$',
        FAQView.as_view(),
        name='faq'
        ),
    url(
        r'^unsubscribe/(?P<pk>\d+)$',
        UnsubscribeEmailUpdateView.as_view(),
        name='unsubscribe'
        ),
    url(
        r'^unsubscribe_confirm/$',
        UnsubscribeConfirmView.as_view(),
        name='unsubscribe_confirm'
        ),
]
