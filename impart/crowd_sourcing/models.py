# -*- coding: utf-8 -*-
"""
Model logic for the Crowd Sourcing application.

.. moduleauthor:: saj
.. versionadded:: Acai_Berry
"""

import logging

from django.conf import settings
from django.core.urlresolvers import reverse
from django.core.validators import MaxLengthValidator
from django.core.validators import validate_email
from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from model_utils.models import TimeStampedModel

from utils.mixins import ChoiceFieldRepresentationsMixin

log = logging.getLogger(__name__)

INSTITUTIONS = (
    ('Dartmouth College / Geisel', 'Dartmouth College / Geisel'),
    ('Dartmouth Hitchcock Medical Center',
        'Dartmouth Hitchcock Medical Center'),
    ('Other', 'Other')
    )
NEED_LAB_OPTIONS = (
    (0, 'No, I do not need laboratory or biometric data.'),
    (1, 'Yes, I do need laboratory or biometric data.')
    )


class ResearchQuestionTopicChoiceField(ChoiceFieldRepresentationsMixin,
                                       TimeStampedModel):
    """
    Dropdown menu list item.

    Represents the form select choices for a question topic (AKA
    "Field of Inquiry") for a :py:class:`ResearchQuestion` object.
    """

    name = models.CharField(max_length=50, unique=True)


class DHMCDeptChoiceField(ChoiceFieldRepresentationsMixin,
                          TimeStampedModel):
    """
    Dropdown menu list item.

    Represents the form select choices for a DHMC department
    for a :py:class:`ResearchQuestion` object.
    """

    name = models.CharField(max_length=50, unique=True)


class GeiselDeptChoiceField(ChoiceFieldRepresentationsMixin,
                            TimeStampedModel):
    """
    Dropdown menu list item.

    Represents the form select choices for a Geisel department
    for a :py:class:`ResearchQuestion` object.
    """

    name = models.CharField(max_length=50, unique=True)


class AllDeptChoiceField(ChoiceFieldRepresentationsMixin,
                         TimeStampedModel):
    """
    Dropdown menu list item.

    Represents the form select choices for departments when the user
    selects an "Other" institution for a :py:class:`ResearchQuestion`
    object.
    """

    name = models.CharField(max_length=50, unique=True)


@python_2_unicode_compatible
class ResearchQuestion(TimeStampedModel):
    """Record the information for a crowd source research question."""

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        db_index=True,
        on_delete=models.CASCADE)
    institution = models.CharField(
        max_length=35, choices=INSTITUTIONS,
        validators=[MaxLengthValidator(35)])
    institution_other = models.CharField(
        max_length=50, blank=True, default='')
    department_dhmc = models.ForeignKey(
        DHMCDeptChoiceField,
        blank=True, null=True, default='',
        on_delete=models.CASCADE)
    department_geisel = models.ForeignKey(
        GeiselDeptChoiceField,
        blank=True, null=True, default='',
        on_delete=models.CASCADE)
    department_all = models.ForeignKey(
        AllDeptChoiceField,
        blank=True, null=True, default='',
        on_delete=models.CASCADE)
    department_other = models.CharField(
        max_length=50, blank=True, default='')
    question = models.TextField(
        max_length=1000, validators=[MaxLengthValidator(1000)])
    population = models.TextField(
        max_length=1000, validators=[MaxLengthValidator(1000)])
    key_exposure_of_interest = models.TextField(
        max_length=1000, validators=[MaxLengthValidator(1000)])
    key_outcome_of_interest = models.TextField(
        max_length=1000, validators=[MaxLengthValidator(1000)])
    need_lab_data = models.BooleanField(choices=NEED_LAB_OPTIONS, default=0)
    need_lab_data_specify = models.TextField(
        max_length=1000, blank=True, default='',
        validators=[MaxLengthValidator(1000)])
    topic = models.ForeignKey(
        ResearchQuestionTopicChoiceField,
        on_delete=models.CASCADE)
    topic_other = models.CharField(
        max_length=50, blank=True, default='')
    ip_address = models.GenericIPAddressField()
    view_count = models.IntegerField(default=0)

    def __str__(self):
        """
        Conversion of object for string representation.

        :returns: String representation of the object.
        :rtype: str
        """

        return self.question

    def __repr__(self):
        """
        Return a representation of the object.

        :returns: Raw representation of the object.
        :rtype: str
        """

        return "{0}, {1}".format(self.id, self.question)

    def __unicode__(self):
        """
        Conversion of object for string representation.

        :returns: Unicode representation of the object.
        :rtype: str
        """

        return unicode(self.__str__())

    def get_absolute_url(self):
        """
        Enable a reverse lookup for list template.

        :returns: The URL associated with the list view of this object.
        :rtype: str
        """

        return reverse('crowd_sourcing:list')

    def get_detail_url(self):
        """
        Enable a reverse lookup for detail template/comment form.

        :returns: The URL associated with the detail view of this object.
        :rtype: str
        """

        return reverse('crowd_sourcing:comment_create', kwargs={'pk': self.pk})

    @classmethod
    def get_questions(self, topic_filter=None, sort_type='newest'):
        """
        Return submitted research questions.

        Return all research questions or all of the questions
        about a specified topic in the specified order.

        :param string topic_filter: The topic with which to filter the queryset
        :param string sort_type: The method for sorting the list
        :returns: Research questions, number of comments, and number of votes
        :rtype: dictionary
        """

        if sort_type == 'votes':
            sort_by = '-total_votes'
        elif sort_type == 'views':
            sort_by = '-view_count'
        else:
            sort_by = '-created'

        if topic_filter is None:
            research_questions = ResearchQuestion.objects.select_related(
                ).annotate(
                total_votes=models.Count(
                    'researchquestionvote', distinct=True)).annotate(
                num_comments=models.Count('researchquestioncomment')).order_by(
                sort_by)
        else:
            research_questions = ResearchQuestion.objects.select_related(
                ).filter(
                topic_id=int(topic_filter)).annotate(
                total_votes=models.Count(
                    'researchquestionvote', distinct=True)).annotate(
                num_comments=models.Count('researchquestioncomment')).order_by(
                sort_by)

        return research_questions

    @classmethod
    def get_question_vote_score(self, question_id):
        """
        Return the number of up-votes and down-votes for a question.

        :param int question_id: Question ID to get the vote score for.
        :returns: question up-votes and down-votes
        :rtype: dictionary
        """

        score_dict = {}

        score_dict['up_votes'] = ResearchQuestionVote.objects.filter(
            question=question_id, vote=1).count()
        score_dict['down_votes'] = ResearchQuestionVote.objects.filter(
            question=question_id, vote=0).count()

        return score_dict

    @classmethod
    def get_existing_vote(self, request, question_id):
        """
        Return the existing vote for question or None if there isn't one.

        :param HTTPRequest request: The request in which to get the user
            ID.
        :param int question_id: Question ID to look up for an existing vote.
        :returns: Question vote value
        :rtype: int
        """

        try:
            current_vote = ResearchQuestionVote.objects.filter(
                user=request.user, question=question_id).values(
                'vote')[0]['vote']
        except IndexError:
            current_vote = None

        return current_vote

    @classmethod
    def get_question_details(self, request, question_id):
        """
        Return the details of a question including up/down votes and existing
        vote.

        :param HTTPRequest request: The request in which to check for
            the vote cookie
        :param int question_id: Question ID to get details for.
        :returns: Question details and up/down votes
        :rtype: dictionary
        """

        question = ResearchQuestion.objects.select_related().get(
            id=question_id)
        votes_dict = ResearchQuestion.get_question_vote_score(
            question.id)
        question.up_votes = votes_dict['up_votes']
        question.down_votes = votes_dict['down_votes']
        question.current_vote = ResearchQuestion.get_existing_vote(
            request, question.id)

        return question


class ResearchQuestionComment(TimeStampedModel):
    """Record the comments for a crowd sourcing research question."""

    user = models.ForeignKey(settings.AUTH_USER_MODEL, db_index=True)
    comment = models.TextField(
        max_length=1000, validators=[MaxLengthValidator(1000)])
    ip_address = models.GenericIPAddressField()
    question = models.ForeignKey(ResearchQuestion)

    def __str__(self):
        """
        Conversion of object for string representation.

        :returns: String representation of the object.
        :rtype: str
        """

        return self.comment

    def __repr__(self):
        """
        Return a representation of the object.

        :returns: Raw representation of the object.
        :rtype: str
        """

        return "{0}, {1}".format(self.id, self.comment)

    def __unicode__(self):
        """
        Conversion of object for string representation.

        :returns: Unicode representation of the object.
        :rtype: str
        """

        return unicode(self.__str__())

    def get_absolute_url(self):
        """
        Enable a reverse lookup for the question list template.

        :returns: The URL associated with the question list.
        :rtype: str
        """

        return reverse(
            'crowd_sourcing:comment_create', kwargs={'pk': self.question.id})

    @classmethod
    def get_comments_for_question(self, question_id):
        """
        Return all comments for a research question.

        :param int question_id: Question ID to get comments for.
        :returns: Question comments
        :rtype: dictionary
        """

        return ResearchQuestionComment.objects.select_related().filter(
            question=question_id).order_by('-created')


class ResearchQuestionVote(TimeStampedModel):
    """
    Record the votes for crowd source research questions.
    """

    user = models.ForeignKey(settings.AUTH_USER_MODEL, db_index=True)
    ip_address = models.GenericIPAddressField()
    vote = models.BooleanField(default=0)
    question = models.ForeignKey(ResearchQuestion)

    def __str__(self):
        """
        Conversion of object for string representation.

        :returns: String representation of the object.
        :rtype: str
        """

        return str(self.vote)

    def __repr__(self):
        """
        Return a representation of the object.

        :returns: Raw representation of the object.
        :rtype: str
        """

        return "{0}, {1}".format(self.id, self.vote)

    def __unicode__(self):
        """
        Conversion of object for string representation.

        :returns: Unicode representation of the object.
        :rtype: str
        """

        return unicode(self.__str__())


class ContactMessage(TimeStampedModel):
    """Save any messages sent via the Contact Form to the database."""

    name = models.CharField(max_length=100)
    email_address = models.EmailField(
        max_length=254, validators=[validate_email, ])
    subject = models.CharField(max_length=100)
    message = models.TextField(
        max_length=1000, validators=[MaxLengthValidator(1000)])
    cc_myself = models.BooleanField(default=False)
    ip_address = models.GenericIPAddressField()

    def __str__(self):
        """
        Conversion of object for string representation.

        :returns: String representation of the object.
        :rtype: str
        """

        return str(self.message)

    def __repr__(self):
        """
        Return a representation of the object.

        :returns: Raw representation of the object.
        :rtype: str
        """

        return "{0}, {1}".format(self.id, self.message)

    def __unicode__(self):
        """
        Conversion of object for string representation.

        :returns: Unicode representation of the object.
        :rtype: str
        """

        return unicode(self.__str__())


class FAQ(TimeStampedModel):
    """Model for storing the questions and answers for the FAQ page."""

    question = models.CharField(
        max_length=1000, validators=[MaxLengthValidator(1000)])
    answer = models.CharField(
        max_length=1000, validators=[MaxLengthValidator(1000)])

    def __str__(self):
        """
        Conversion of object for string representation.

        :returns: String representation of the object.
        :rtype: str
        """

        return str(self.question)

    def __repr__(self):
        """
        Return a representation of the object.

        :returns: Raw representation of the object.
        :rtype: str
        """

        return "{0}, {1}".format(self.id, self.question)

    def __unicode__(self):
        """
        Conversion of object for string representation.

        :returns: Unicode representation of the object.
        :rtype: str
        """

        return unicode(self.__str__())

    def get_first_three(self):
        """Return first three FAQs.

        :returns: Records for first 3 FAQs
        :rtype: QuerySet
        """

        return FAQ.objects.all()[:3]
