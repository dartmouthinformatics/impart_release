# -*- coding: utf-8 -*-
"""
Routing table for the overall project.
"""

from django.conf import settings
from django.conf.urls import include
from django.conf.urls import url
from django.contrib import admin

from crowd_sourcing.views import HomePageDetailView

admin.autodiscover()


urlpatterns = []
if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(
            r'^__debug__/',
            include(debug_toolbar.urls)
        ),
    ]

urlpatterns += [
    url(r'^$',
        HomePageDetailView.as_view(), name='index'),
    url(r'^accounts/',
        include('authentication.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^crowd_sourcing/',
        include('crowd_sourcing.urls')),
    url(r'^crowd_sourcing_open/',
        include('crowd_sourcing_open.urls')),
    url(r'^comm_crowd_sourcing/',
        include('comm_crowd_sourcing.urls')),
    url(r'^signed/',
        include('signed_link.urls')),
    url(r'^autocomplete/',
        include('autocomplete_light.urls')),
    url('',
        # include('social.apps.django_app.urls')),
        include('social.apps.django_app.urls', namespace='social')),
]
